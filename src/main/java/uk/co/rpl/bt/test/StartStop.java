/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

/**
 *
 * @author philip
 */
public enum StartStop {
	START("Start"),STOP("End");
	String name;
	StartStop(String name){
		this.name = name;
	}

	public static StartStop get(String name) throws InvalidInputException{
		for (StartStop value: values()){
			if (value.name.equals(name)) return value;
		}
		throw new InvalidInputException("Start/Stop name "+name+" is unknown");
	}
}
