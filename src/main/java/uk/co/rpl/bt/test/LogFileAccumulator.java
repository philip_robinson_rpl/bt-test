/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.OptionalInt;

/**
 *
 * @author philip
 */
public class LogFileAccumulator {

	private LogReader reader;

	public LogFileAccumulator(LogReader reader) {
		this.reader = reader;
	}

	public Collection<CustomerState> gather() throws IOException {
		Map<String, CustomerState> customerToState = new HashMap<>();
		reader.read((start, line) -> {
			String name = line.getCustomer();
			if (!customerToState.containsKey(name)) {
				customerToState.put(name, new CustomerState(name, start));
			}
			switch (line.getType()) {
			case START:
				customerToState.get(name).start(line.getTimeStamp());
				break;
			case STOP:
				customerToState.get(name).stop(line.getTimeStamp());
				break;
			}
		});
		OptionalInt lastEntry = customerToState.values().stream().
			mapToInt(e->e.getLastTimestamp()).max();
		if (lastEntry.isPresent())customerToState.values().
			forEach(state -> state.stopAll(lastEntry.getAsInt()));
		return customerToState.values();
	}

}
