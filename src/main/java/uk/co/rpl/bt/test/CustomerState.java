/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

/**
 *
 * @author philip
 */
public class CustomerState {

	private int numberOfSeconds;
	private int lastTimestamp;
	private final int startTime;
	private int activeSessions;
	private final String name;
	private int closedSessions;

	public CustomerState(String name, int startTime) {
		this.name = name;
		lastTimestamp = startTime;
		this.startTime = startTime;
	}

	public void start(int time) {
		final int additional = (time - lastTimestamp) * activeSessions;
		numberOfSeconds += additional;
		lastTimestamp = time;
		activeSessions += 1;
	}

	public void stop(int time) {
		if (activeSessions == 0) {
			numberOfSeconds += time - startTime;
		} else {
			final int additional = (time - lastTimestamp) * activeSessions;
			numberOfSeconds += additional;
			activeSessions -= 1;
		}
		closedSessions+=1;
		lastTimestamp = time;
	}

	public void stopAll(int time) {
			final int additional = (time - lastTimestamp) * activeSessions;
		numberOfSeconds += additional;
		lastTimestamp = time;
		closedSessions +=activeSessions;
		activeSessions = 0;
	}

	public int getNumberOfSeconds() {
		return numberOfSeconds;
	}

	public int getClosedSessions() {
		return closedSessions;
	}

	public String getName() {
		return name;
	}

	public int getLastTimestamp() {
		return lastTimestamp;
	}

}
