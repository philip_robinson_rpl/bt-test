/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

import java.io.File;
import java.io.IOException;
import static java.lang.System.out;

/**
 *
 * @author philip
 */
public class BTTest {

	public static void main(String[] argv) throws IOException {
		if (argv.length != 1) {
			out.println("Usage java -jar bt-test.jar filename_of_log_file");
		} else {
			File file = new File(argv[0]);
			LogReader reader = new LogReader(file);
			LogFileAccumulator accumulator = new LogFileAccumulator(reader);
			accumulator.gather().forEach(reportLine -> out.printf(
				"%20s %8d %8d\n", reportLine.getName(),
				reportLine.getClosedSessions(),
				reportLine.getNumberOfSeconds()));
		}
	}
}
