/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.out;
import java.util.function.BiConsumer;

/**
 * This class reads a log file and generates a sequence of Billing Records
 *
 * @author philip
 */
public class LogReader {

	private final File file;

	public LogReader(File file) {
		this.file = file;
	}

	/**
	 * Parse the time field and return the number of seconds represented.
	 *
	 * @param timeString the time string as hours:minutes:seconds
	 * @return the time represented by the input string as a number of seconds
	 * @throws InvalidInputException if the input string cannot be parsed
	 */
	private int getTime(String timeString) throws InvalidInputException {
		String[] fields = timeString.split(":");
		if (fields.length != 3) {
			throw new InvalidInputException(
				"not correct number of time fields");
		}
		try {
			int hrs = Integer.parseInt(fields[0]);
			int mins = Integer.parseInt(fields[1]);
			int secs = Integer.parseInt(fields[2]);
			if (secs > 59 || secs < 0) {
				throw new InvalidInputException(
					"Invalid seconds fields");
			}
			if (mins > 59 || mins < 0) {
				throw new InvalidInputException(
					"Invalid minutes fields");
			}
			if (hrs < 0) {
				throw new InvalidInputException(
					"Invalid hours fields");
			}
			return secs + 60 * (mins + 60 * hrs);
		} catch (NumberFormatException e) {
			throw new InvalidInputException(e);
		}
	}

	/**
	 * parse the start stop field
	 *
	 * @param startStopString the input string which should be the word start or
	 * stop
	 * @return START or STOP depending on the input
	 * @throws InvalidInputException thrown if the given string is not valid
	 */
	private StartStop getStartStop(String startStopString)
		throws InvalidInputException {
		return StartStop.get(startStopString);
	}

	/**
	 * read log file an find all valid rows
	 *
	 * @param callback this is called with each valid line in the log file.
	 * @throws IOException thrown if the log file could not be read
	 */
	public void read(BiConsumer<Integer, BillingTimestamp> callback) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			BillingTimestamp entry1 = null;
			while ((line = br.readLine()) != null) {
				entry1 = getBillingEntry(line);
				if (entry1 != null){
					callback.accept(entry1.timeStamp, entry1);
					break;
				}
			}
			while ((line = br.readLine()) != null) {
				BillingTimestamp entry = getBillingEntry(line);
				if (entry != null){
					callback.accept(entry1.timeStamp, entry);
				}
			}
		}
	}

	private BillingTimestamp getBillingEntry(String line) {
		String[] fields = line.split("\\s+");
		try {
			if (fields.length != 3) {
				throw new InvalidInputException(
					"Incorrect number of fields: " + fields.length);
			}
			int time = getTime(fields[0]);
			StartStop type = getStartStop(fields[2]);
			return new BillingTimestamp(time,
				fields[1], type);
		} catch (InvalidInputException e) {
			return null;
		}
	}

	public class BillingTimestamp {

		private final int timeStamp;
		private final String customer;
		private final StartStop type;

		public BillingTimestamp(int timeStamp, String customer, StartStop type) {
			this.timeStamp = timeStamp;
			this.customer = customer;
			this.type = type;
		}

		public int getTimeStamp() {
			return timeStamp;
		}

		public String getCustomer() {
			return customer;
		}

		public StartStop getType() {
			return type;
		}

		@Override
		public String toString() {
			return "BillingTimestamp{" + "timeStamp=" + timeStamp + 
				", customer=" + customer + ", type=" + type + '}';
		}

	}
}
