/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import org.junit.Test;
import static org.junit.Assert.*;
import static uk.co.rpl.bt.test.StartStop.START;
import static uk.co.rpl.bt.test.StartStop.STOP;

/**
 *
 * @author philip
 */
public class LogFileAccumulatorTest {
	
	public LogFileAccumulatorTest() {
	}
	

	/**
	 * Test of gather method, of class LogFileAccumulator.
	 */
	@Test
	public void testGatherSimple() throws Exception {
		System.out.println("testGatherSimple");

		LogReader reader = new LogReader(null){
			@Override
			public void read(BiConsumer<Integer, BillingTimestamp> callback) {
				callback.accept(100, new BillingTimestamp(200, "FRED", START));
				callback.accept(100, new BillingTimestamp(210, "JOE", START));
				callback.accept(100, new BillingTimestamp(320, "FRED", STOP));
				callback.accept(100, new BillingTimestamp(440, "JOE", STOP));
			}
		};
		LogFileAccumulator instance = new LogFileAccumulator(reader);
		Collection<CustomerState> result = instance.gather();
		assertEquals(2, result.size());
		Map<String, CustomerState> res = new HashMap<>();
		for (CustomerState cs: result) res.put(cs.getName(), cs);
		assertEquals(1, res.get("FRED").getClosedSessions());
		assertEquals(120, res.get("FRED").getNumberOfSeconds());
		assertEquals(1, res.get("JOE").getClosedSessions());
		assertEquals(230, res.get("JOE").getNumberOfSeconds());
	}
	
	@Test
	public void testGatherCrossingBoundary() throws Exception {
		System.out.println("testGatherCrossingBoundary");

		LogReader reader = new LogReader(null){
			@Override
			public void read(BiConsumer<Integer, BillingTimestamp> callback) {
				callback.accept(100, new BillingTimestamp(200, "FRED", STOP));
				callback.accept(100, new BillingTimestamp(320, "FRED", START));
				callback.accept(100, new BillingTimestamp(470, "MARY", START));
			}
		};
		LogFileAccumulator instance = new LogFileAccumulator(reader);
		Collection<CustomerState> result = instance.gather();
		assertEquals(2, result.size());
		Map<String, CustomerState> res = new HashMap<>();
		for (CustomerState cs: result) res.put(cs.getName(), cs);
		assertEquals(2, res.get("FRED").getClosedSessions());
		assertEquals(250, res.get("FRED").getNumberOfSeconds());
		assertEquals(1, res.get("MARY").getClosedSessions());
		assertEquals(0, res.get("MARY").getNumberOfSeconds());
	}
	
}
