/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author philip
 */
public class CustomerStateTest {
	
	public CustomerStateTest() {
	}
	
	/**
	 * Test of getName method, of class CustomerState.
	 */
	@Test
	public void testNormalSession() {
		System.out.println("normalSession");
		CustomerState inst = new CustomerState("FRED", 7000);
		assertEquals("FRED", inst.getName());
		assertEquals(0, inst.getNumberOfSeconds());
		inst.start(8000);
		assertEquals(0, inst.getNumberOfSeconds());
		inst.stop(9000);
		assertEquals(1000, inst.getNumberOfSeconds());
		inst.stopAll(10000);
		assertEquals(1000, inst.getNumberOfSeconds());
		assertEquals(1, inst.getClosedSessions());
	}
	@Test
	public void testAlreadyStarted() {
		System.out.println("alreadyStarted");
		CustomerState inst = new CustomerState("FRED", 7000);
		assertEquals(0, inst.getNumberOfSeconds());
		inst.stop(9000);
		assertEquals(2000, inst.getNumberOfSeconds());
		inst.stopAll(10000);
		assertEquals(2000, inst.getNumberOfSeconds());
		assertEquals(1, inst.getClosedSessions());
	}
	@Test
	public void testNotEnded() {
		System.out.println("notEnded");
		CustomerState inst = new CustomerState("FRED", 7000);
		assertEquals(0, inst.getNumberOfSeconds());
		inst.start(8000);
		assertEquals(0, inst.getNumberOfSeconds());
		inst.stopAll(10000);
		assertEquals(2000, inst.getNumberOfSeconds());
		assertEquals(1, inst.getClosedSessions());
	}

	@Test
	public void testParallelWithUnopened(){
		System.out.println("testParallelWithUnopened");
		CustomerState inst = new CustomerState("FRED", 7000);
		assertEquals(0, inst.getNumberOfSeconds());
		inst.start(8000);
		assertEquals(0, inst.getNumberOfSeconds());
		inst.stop(8500);
		assertEquals(500, inst.getNumberOfSeconds());
		inst.stop(8600);
		assertEquals(2100, inst.getNumberOfSeconds());
		inst.stopAll(10000);
		assertEquals(2100, inst.getNumberOfSeconds());
		assertEquals(2, inst.getClosedSessions());
	}

	@Test
	public void testZero(){
		System.out.println("zero");
		CustomerState inst = new CustomerState("FRED", 7000);
		assertEquals(0, inst.getNumberOfSeconds());
		inst.start(10000);
		inst.stopAll(10000);
		assertEquals(0, inst.getNumberOfSeconds());
		assertEquals(1, inst.getClosedSessions());
	}
	
}
