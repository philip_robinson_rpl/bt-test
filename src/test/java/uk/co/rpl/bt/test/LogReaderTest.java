/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.rpl.bt.test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author philip
 */
public class LogReaderTest {

	/**
	 * Test of read method, of class LogReader.
	 */
	@Test
	public void testRead() throws Exception {
		List<LogReader.BillingTimestamp> list = new ArrayList<>();
		Set<String> names = new HashSet<>();
		Set<Integer> starts = new HashSet<>();
		LogReader inst;
		URL url = getClass().getResource("/log-test.log");

		File f = new File(url.getFile());
		inst = new LogReader(f);
		inst.read((start, line) -> {
			starts.add(start);
			list.add(line);
			names.add(line.getCustomer());
		});
		assertEquals(11, list.size());
		assertEquals(2, names.size());
		assertEquals(1, starts.size());
		assertEquals((14*60+2)*60+3, starts.iterator().next().intValue());
	}

}
