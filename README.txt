

BTTest


This application is written in Java 1.8 - specifically tested with 1.8.0_73 and 
9-ea+102-2016-01-21-003129.javare.4316.nc

The main class is uk.co.rpl.bt.test.BTTest which takes a single file path
for a file that will be read as a log file.

The file is expected to contain lines that have 3 space separated fields

timestamp  customer_name  action

* timestamp is three colon separated numeric fields hours:minutes:seconds
	It is assumed that all timestamps are from one day and that they are in 
	order
* customer_name is a customer name which cannot contain spaces and is 
	case sensitive
* action is Start or End and is case sensitive and indicates the 
	Start or End of a session.

Any lines that do not conform to the above will be ignored.

*Sessions that do not have a start are assumed to start at the time of the
	first entry in the file.
*Sessions that do not have an end entry will be assumed to end at the timestamp
	of the last entry on the file.

Time is counted from the start timestamp to the end timestamp but not including
the end timestamp, so if start and end are the same the duration is 0.

If the file does not exist or cannot be read an exception will be printed 
to the standard out, otherwise a customer use report will be printed.

     customer_name   no_of_sessions   total_number_of_seconds

i.e.

     Fred        17     568
      Joe         3      10

Formatting is applied to this report which right justifies the data
into columns for easier reading.


Some tests are included which use Junit4




Building the application

Any IDE should happily build this but a maven pom is included to allow
a maven build to be performed.

If maven is used to build this there will then be a jar in the target directory

	target/BT-TEST-1.0-SNAPSHOT.jar

This may be executed for a single log file using the command line

	$> java -classpath target/BT-TEST-1.0-SNAPSHOT.jar uk.co.rpl.bt.test.BTTest file 

The included script exec.sh may be used to do this provided an appropriate
java runtime is on the path.

	$> ./exec.sh log-file-name


